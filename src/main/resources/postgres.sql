CREATE DATABASE writly
    WITH 
    OWNER = michal
    ENCODING = 'UTF8'
    LC_COLLATE = 'pl_PL.UTF-8'
    LC_CTYPE = 'pl_PL.UTF-8'
    CONNECTION LIMIT = -1;
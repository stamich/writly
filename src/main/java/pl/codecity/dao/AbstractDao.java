package pl.codecity.dao;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import java.io.Serializable;
import java.lang.reflect.ParameterizedType;

public abstract class AbstractDao<PK extends Serializable, T>{

    @Autowired
    private SessionFactory sessionFactory;

    private final Class<T> persistentClass;

    @SuppressWarnings("unchecked")
    public AbstractDao() {
        this.persistentClass = (Class<T>) ((ParameterizedType) this.getClass().getGenericSuperclass()).getActualTypeArguments()[1];
    }

    protected Session getSession(){
        return sessionFactory.getCurrentSession();
    }

    @SuppressWarnings("unchecked")
    public T getByKey(PK key){
        return (T) getSession().get(persistentClass, key);
    }

    public void persistEntity(T entity){
        getSession().persist(entity);
    }

    public void updateEntity(T entity){
        getSession().update(entity);
    }

    public void deleteEntity(T entity){
        getSession().delete(entity);
    }

    protected Criteria createEntityCriteria(){
        CriteriaBuilder builder = getSession().getCriteriaBuilder();
        CriteriaQuery<T> query = builder.createQuery(persistentClass);
        //Root<T> root = query.from(persistentClass);
        return (Criteria) getSession().createQuery(query).getResultList();
    }

    // deprecated

//    protected Criteria createEntityCriteria(){
//        return getSession().createCriteria(persistentClass);
//    }

}

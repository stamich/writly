package pl.codecity.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import pl.codecity.helper.ArticleSearchHelper;
import pl.codecity.model.Article;

import java.util.List;

public interface ArticleRepositoryCustom {

    Page<Article> search(ArticleSearchHelper helper);
    Page<Article> search(ArticleSearchHelper helper, Pageable pageable);
    List<Long> searchForId(ArticleSearchHelper helper);
}

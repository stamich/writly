package pl.codecity.repository;

import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.Session;
import org.hibernate.search.FullTextQuery;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;
import pl.codecity.helper.ArticleSearchHelper;
import pl.codecity.model.Article;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Repository("articleRepository")
public class ArticleRepositoryImpl implements ArticleRepositoryCustom {

    @PersistenceContext
    private EntityManager manager;

    @Override
    public Page<Article> search(ArticleSearchHelper helper) {
        return search(helper, Pageable.unpaged());
    }

    @Override
    public Page<Article> search(ArticleSearchHelper helper, Pageable pageable) {
        Session session = (Session) manager.getDelegate();
        Criteria criteria = session.createCriteria(Article.class)
                .setFetchMode("cover", FetchMode.JOIN)
                .setFetchMode("user", FetchMode.JOIN)
                .setFetchMode("categories", FetchMode.JOIN)
                .setFetchMode("tags", FetchMode.JOIN);

        FullTextQuery persistenceQuery = buildFullTextQuery(helper, pageable, criteria);
        int resultSize = persistenceQuery.getResultSize();
        List<Article> results = persistenceQuery.getResultList();
        return new PageImpl<>(results, pageable, resultSize);
    }

    @Override
    public List<Long> searchForId(ArticleSearchHelper helper) {
        return null;
    }

    private FullTextQuery buildFullTextQuery(ArticleSearchHelper helper, Pageable pageable, Criteria criteria){

        return null;
    }
}

package pl.codecity.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.codecity.model.Comment;

@Repository
public interface CommentRepository extends JpaRepository<Comment, Long> {

    Comment findOne(Long id);

    Comment findOneById(Long id);
}

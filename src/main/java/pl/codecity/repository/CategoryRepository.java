package pl.codecity.repository;

import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import pl.codecity.model.Category;

import java.util.List;

@Repository
public interface CategoryRepository extends JpaRepository<Category, Long> {

    Category findOneById(Long id);

    Category findOneByIdAndLanguage(Long id, String language);

    Category findOneByCodeAndLanguage(String code, String language);

    List<Category> findAll(Specification<Category> spec, Sort sort);

    List<Category> findAllDistinctByLanguageOrderByLeft_Asc(String language);

    @Query("SELECT COALESCE(MAX(right_), 0) FROM Category ")
    int findMaxRight();

    @Modifying
    @Query("UPDATE Category SET left_ = left_ + 2 WHERE left_ > :right_ ")
    void unshiftLeft(@Param("right_") int right_);

    @Modifying
    @Query("UPDATE Category SET right_ = right_ + 2 WHERE right_ >= :right_ ")
    void unshiftRight(@Param("right_") int right_);

    @Modifying
    @Query("UPDATE Category SET right_ = right_ - 1, left_ = left_ - 1 WHERE left_ BETWEEN :left_ AND :right_ ")
    void shiftLeftRight(@Param("left_") int left_, @Param("right_") int right_);

    @Modifying
    @Query("UPDATE Category SET left_ = left_ - 2 WHERE left_ > :right_ ")
    void shiftLeft(@Param("right_") int right_);

    @Modifying
    @Query("UPDATE Category SET right_ = right_ - 2 WHERE right_ > :right_ ")
    void shiftRight(@Param("right_") int right_);

}

package pl.codecity.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Lock;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import pl.codecity.model.Tag;

import javax.persistence.LockModeType;
import java.util.List;

public interface TagRepository extends JpaRepository<Tag, Long> {

    Tag findOneByIdAndLanguage(Long id, String language);

    @Lock(LockModeType.PESSIMISTIC_WRITE)
    Tag findOneForUpdateByIdAndLanguage(Long id, String language);

    Tag findOneByNameAndLanguage(String name, String language);

    @Lock(LockModeType.PESSIMISTIC_WRITE)
    Tag findOneForUpdateByNameAndLanguage(String name, String language);

    List<Tag> findAllByLanguage(String language);

    @Query("SELECT COUNT(tag.id) FROM Tag t WHERE t.language = :language ")
    long count(@Param("language") String language);
}

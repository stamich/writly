package pl.codecity.repository;

import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import pl.codecity.model.Article;
import pl.codecity.model.Post;

import java.util.List;
import java.util.Map;

@Repository
public interface ArticleRepository extends JpaRepository<Article, Long>, ArticleRepositoryCustom {

    Article findOne(Specification<Article> specification);

    Article findOneById(Long id);

    Article findOneByIdAndLanguage(Long id, String language);

    @Query("SELECT COUNT (Article.id) FROM Article a WHERE a.language = :language AND a.drafted IS NULL ")
    long countByLanguage(@Param("language") String language);

    @Query("SELECT COUNT (Article.id) FROM Article a WHERE a.status = :status AND a.language = :language AND a.drafted IS NULL ")
    long countByStatus(@Param("status") Post.Status status, @Param("language") String language);

    @Query("SELECT NEW map(User.id AS userId, COUNT(Article.id) AS count) FROM Article a " +
            "LEFT JOIN a.author user " +
            "WHERE a.status = :status AND a.language = :language " +
            "GROUP BY user.id ")
    List<Map<String, Object>> countByAuthorIdGrouped(@Param("status") Post.Status status, @Param("language") String language);

    @Query("SELECT NEW map(category.id AS categoryId, COUNT(article.id) AS count) FROM Article a " +
                    "LEFT JOIN a.categories category " +
                    "WHERE a.status = :status AND a.language = :language " +
                    "GROUP BY category.id ")
    List<Map<String, Object>> countByCategoryIdGrouped(@Param("status") Post.Status status, @Param("language") String language);

    @Query("SELECT NEW map(tag.id AS tagId, COUNT(article.id) AS count) FROM Article a " +
                    "LEFT JOIN a.tags tag " +
                    "WHERE a.status = :status AND a.language = :language " +
                    "GROUP BY tag.id ")
    List<Map<String, Object>> countByTagIdGrouped(@Param("status") Post.Status status, @Param("language") String language);

    @Modifying
    @Query("DELETE FROM Article a WHERE a.drafted = :drafted ")
    void deleteByDrafted(@Param("drafted") Article drafted);
}

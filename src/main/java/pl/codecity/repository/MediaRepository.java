package pl.codecity.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.codecity.model.Media;

public interface MediaRepository extends JpaRepository<Media, String> {

    Media findOne(String id);
}

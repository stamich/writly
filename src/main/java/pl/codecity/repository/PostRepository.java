package pl.codecity.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import pl.codecity.model.Post;

import java.time.LocalDateTime;
import java.util.List;

public interface PostRepository extends JpaRepository<Post, Long> {

    Post findOneByIdAndLanguage(Long id, String language);

    Post findOneByCodeAndLanguage(String code, String language);

    List<Post> findAllByStatusAndDateLessThanEqual(Post.Status status, LocalDateTime date);

    @Query("SELECT COUNT(post.id) FROM Post p WHERE p.language = :language ")
    long count(@Param("language") String language);

    @Query("SELECT COUNT(post.id) FROM Post p WHERE p.status = :status and p.language = :language ")
    long countByStatus(@Param("status") Post.Status status, @Param("language") String language);
}

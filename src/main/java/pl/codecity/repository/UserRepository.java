package pl.codecity.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Lock;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import pl.codecity.model.User;

import javax.persistence.LockModeType;
import java.util.Collection;
import java.util.Date;
import java.util.List;

public interface UserRepository extends JpaRepository<User, Long> {

    User findOneById(Long id);

    @Lock(LockModeType.PESSIMISTIC_WRITE)
    User findOneForUpdateById(Long id);

    User findOneByLoginId(String loginId);

    User findOneByEmail(String email);

    List<User> findAllByIdIn(Collection<Long> ids);

    @Modifying
    @Query("UPDATE User SET lastLoginTime = :lastLoginTime WHERE id = :id ")
    int updateLastLoginTime(@Param("id") long id, @Param("lastLoginTime") Date lastLoginTime);
}

package pl.codecity.service;

import org.springframework.data.jpa.domain.Specification;
import pl.codecity.model.Article;
import pl.codecity.model.Post;

import java.util.List;
import java.util.Map;

public interface ArticleService {

    Article findOne(Specification<Article> specification);

    Article findOneById(Long id);

    Article findOneByIdAndLanguage(Long id, String language);

    long countByLanguage (String language);

    long countByStatus(Post.Status status, String language);

    List<Map<String, Object>> countByAuthorIdGrouped(Post.Status status, String language);

    List<Map<String, Object>> countByCategoryIdGrouped(Post.Status status, String language);

    List<Map<String, Object>> countByTagIdGrouped(Post.Status status, String language);

    void deleteByDrafted(Article drafted);
}

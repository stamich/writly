package pl.codecity.model;

import org.apache.commons.lang3.builder.CompareToBuilder;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = "comment")
@DynamicInsert
@DynamicUpdate
@SuppressWarnings("serial")
public class Comment extends AbstractDomainObject<Long> implements Comparable<Comment>{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    private Post post;

    @ManyToOne(fetch = FetchType.LAZY)
    //@IndexedEmbedded(includeEmbeddedObjectId = true)
    private User author;

    @Column(length = 200, nullable = false)
    private String authorName;

    @Column(nullable = false)
    private LocalDateTime date;

    @Lob
    @Column(nullable = false)
    private String content;

    @Column(nullable = false)
    private boolean approved;

    @Override
    public Long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Post getPost() {
        return post;
    }

    public void setPost(Post post) {
        this.post = post;
    }

    public User getAuthor() {
        return author;
    }

    public void setAuthor(User author) {
        this.author = author;
    }

    public String getAuthorName() {
        return authorName;
    }

    public void setAuthorName(String authorName) {
        this.authorName = authorName;
    }

    public LocalDateTime getDate() {
        return date;
    }

    public void setDate(LocalDateTime date) {
        this.date = date;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public boolean isApproved() {
        return approved;
    }

    public void setApproved(boolean approved) {
        this.approved = approved;
    }

    @Override
    public String print() {
        return this.getClass().getName() + " " + getId();
    }

    public int compareTo(Comment comment) {
        return new CompareToBuilder()
                .append(getDate(), comment.getDate())
                .append(getId(), comment.getId())
                .toComparison();
    }
}

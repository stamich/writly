package pl.codecity.model;

import java.io.Serializable;

@SuppressWarnings("serial")
public enum PostStatus implements Serializable {

    DRAFT("DRAFT"),
    SCHEDULED("SCHEDULED"),
    PUBLISHED("PUBLISHED");

    private String postStatus;

    PostStatus(String postStatus) {
        this.postStatus = postStatus;
    }

    public String getPostStatus() {
        return postStatus;
    }
}

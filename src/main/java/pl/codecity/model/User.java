package pl.codecity.model;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.SortNatural;

import javax.persistence.*;
import java.util.SortedSet;
import java.util.TreeSet;

@Entity
@DynamicInsert
@DynamicUpdate
@Table(name = "`user`")
@SuppressWarnings("serial")
public class User extends AbstractDomainObject<Long>{

    public enum Role {
        ADMIN,
        EDITOR,
        AUTHOR,
        VIEWER,
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(length = 100, nullable = false, unique = true)
    private String loginId;

    @Column(length = 500, nullable = false)
    private String loginPassword;

    @Embedded
    private PersonalName name = new PersonalName();

    @Column(length = 500)
    private String nickname;

    @Column(length = 200, nullable = false, unique = true)
    private String email;

    @Lob
    @Column
    private String description;

    @ElementCollection
    @SortNatural
    @JoinTable(name = "user_role")
    @Enumerated(EnumType.STRING)
    @Column(name = "role", length = 20, nullable = false)
    private SortedSet<Role> roles = new TreeSet<>();

    //

    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLoginId() {
        return loginId;
    }

    public void setLoginId(String loginId) {
        this.loginId = loginId;
    }

    public String getLoginPassword() {
        return loginPassword;
    }

    public void setLoginPassword(String loginPassword) {
        this.loginPassword = loginPassword;
    }

    public PersonalName getName() {
        return name;
    }

    public void setName(PersonalName name) {
        this.name = name;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public SortedSet<Role> getRoles() {
        return roles;
    }

    public void setRoles(SortedSet<Role> roles) {
        this.roles = roles;
    }

    //

    @Override
    public String print() {
        return null;
    }
}

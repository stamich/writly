package pl.codecity.model;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.SortNatural;

import javax.persistence.*;
import java.util.List;
import java.util.SortedSet;
import java.util.TreeSet;

@Entity
@DynamicInsert
@DynamicUpdate
@Table(name = "category")
@SuppressWarnings("serial")
public class Category extends AbstractDomainObject<Long> implements Comparable<Category> {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(length = 200, nullable = false)
    private String code;

    @Column(length = 3, nullable = false)
    private String language;

    @Column(length = 200, nullable = false)
    private String name;

    @Lob
    private String description;

    @Column(nullable = false)
    private int left_;

    @Column(nullable = false)
    private int right_;

    @ManyToOne
    private Category parent;

    @OneToMany(mappedBy = "parent", cascade = CascadeType.ALL)
    private List<Category> children;

    @ManyToMany
    @JoinTable(name = "post_category", joinColumns = {@JoinColumn(name = "category_id")},
            inverseJoinColumns = @JoinColumn(name = "post_id", referencedColumnName = "id"))
    @SortNatural
    private SortedSet<Post> posts = new TreeSet<>();

//	@Formula("(" +
//			"select count(distinct article.id) from article article " +
//			"inner join post post on article.id = post.id " +
//			"inner join article_category category on article.id = category.article_id " +
//			"where category.category_id = id " +
//			"and post.status = 'PUBLISHED') ")
//	private int articleCount;

    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getLeft_() {
        return left_;
    }

    public void setLeft_(int left_) {
        this.left_ = left_;
    }

    public int getRight_() {
        return right_;
    }

    public void setRight_(int right_) {
        this.right_ = right_;
    }

    public Category getParent() {
        return parent;
    }

    public void setParent(Category parent) {
        this.parent = parent;
    }

    public List<Category> getChildren() {
        return children;
    }

    public void setChildren(List<Category> children) {
        this.children = children;
    }

    public SortedSet<Post> getPosts() {
        return posts;
    }

    public void setPosts(SortedSet<Post> posts) {
        this.posts = posts;
    }

//	public int getArticleCount() {
//		return articleCount;
//	}

    @Override
    public String print() {
        return getName();
    }

    @Override
    public String toString() {
        return getName();
    }

    @Override
    public int compareTo(Category category) {
        int leftDiff = getLeft_() - category.getLeft_();
        if (leftDiff != 0) {
            return leftDiff;
        }
        return (int) (category.getId() - getId());
    }
}
